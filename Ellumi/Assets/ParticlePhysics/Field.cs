using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class Field : MonoBehaviour
{

    protected Element[,] cellMap;
    public int sizeX;
    public int sizeY;
    public TileBase[] tiles;
    public Tilemap tilemap;
    public Transform EllumiSpawner;
    private float nextTickTimer = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        cellMap = new Element[sizeY, sizeX];
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                Element newElement = ScriptableObject.CreateInstance<SleepingAir>();
                newElement.Setup(this, x, y);
                cellMap[y, x] = newElement;
            }
        }

        for (int y = 100; y < 120; y++) {
            for (int x = 100; x < 280; x++) {
                Element newElement = ScriptableObject.CreateInstance<SleepingWood>();
                newElement.Setup(this, x, y);
                cellMap[y, x] = newElement;
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        nextTickTimer += Time.fixedDeltaTime;
        if (nextTickTimer < 1f / 24f) { return; }
        nextTickTimer = 0;
        
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                Element cell = cellMap[y, x];
                if (cell) {
                    cell.ProcessCell();
                }
            }
        }
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                Element cell = cellMap[y, x];
                if (cell) {
                    cell.UpdateToNextState();
                    cell.UnmarkUpdated();
                }
            }
        }
        Vector3Int spawn = tilemap.WorldToCell(EllumiSpawner.position);
        for (int i = 0; i < 3; i++) {
            ReplaceElementAtPos(Element.CreateNewElement<Ember>(this), spawn.x + Random.Range(-5, 6),
                spawn.y + Random.Range(-5, 6) + 5);
            ReplaceElementAtPos(Element.CreateNewElement<Ember>(this), spawn.x + Random.Range(-5, 6),
                spawn.y + Random.Range(-5, 6) - 6);
        }
        
    }

    private void Update()
    {
        RenderToTileMap(tilemap);
        Element f = Element.CreateNewElement<Flame>(this);
    }

    void RenderToTileMap(Tilemap tilemap)
    {
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                Element cell = cellMap[y,x];
                if (cell) {
                    tilemap.SetTile(new Vector3Int(x, y, 0), tiles[cell.tileGraphicIndex]);
                } else {
                    tilemap.SetTile(new Vector3Int(x, y, 0), null);
                }
            }
        }
        
    }

    public Element GetElementAtPos(int x, int y)
    {
        if (x >= 0 && x < sizeX && y >= 0 && y < sizeY) {
            return cellMap[y, x];
        }
        return Element.CreateNewElement<OutOfBounds>(this);
    }

    public Element[] GetNeighborsOf(int x, int y)
    {
        Element[] result = new Element[4];
        result[0] = GetElementAtPos(x, y - 1);
        result[1] = GetElementAtPos(x - 1, y);
        result[2] = GetElementAtPos(x + 1, y);
        result[3] = GetElementAtPos(x, y + 1);
        return result;
    }

    public void ReplaceElementAtPos(Element newElement,int x, int y)
    {
        newElement.SetPos(x, y);
        cellMap[y, x] = newElement;
    }
}
