using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Air : Element
{
    const int MY_GRAPHIC_INDEX = 0;

    private int inactiveTurns;

    override public void Setup(Field field, int x = 0, int y = 0, int graphic = 0)
    {
        elementID = 2;
        inactiveTurns = 0;
        base.Setup(field, x, y, MY_GRAPHIC_INDEX);
    }

    // Update is called once per frame
    override public void ProcessCell()
    {
        Element[] neighbors = field.GetNeighborsOf(x, y);
        float avgHeat = (neighbors[0].GetHeat() + neighbors[1].GetHeat() + neighbors[2].GetHeat() +
                            neighbors[3].GetHeat()) / 4f;
        heat = (int)(heat * 0.75f + avgHeat * 0.25);
        
        if (heat > 2) {
            ReplaceSelf<Flame>();
        }
        else if (heat > 10) {
            ReplaceSelf<Fire>();
        }
        else if (heat > 15) {
            ReplaceSelf<Flame>();
        }

        heat += heat < 0? 1 : -1;

        if (heat == 0) {
            inactiveTurns += 1;
        } else {
            inactiveTurns = 0;
        }
        if (inactiveTurns > 7) {
            ReplaceSelf<SleepingAir>();
        }  
    }
}
