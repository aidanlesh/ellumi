using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounds : Element
{
    const int MY_GRAPHIC_INDEX = 0;

    override public void Setup(Field field, int x = 0, int y = 0, int graphic = 0)
    {
        elementID = -1;
        base.Setup(field, x, y, MY_GRAPHIC_INDEX);
    }
}
