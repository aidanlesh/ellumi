using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flame : Element
{
    const int MY_GRAPHIC_INDEX = 2;

    override public void Setup(Field field, int x = 0, int y = 0, int graphic = 0)
    {
        elementID = 1;
        base.Setup(field, x, y, MY_GRAPHIC_INDEX);
    }

    override public void ProcessCell()
    {
        Element[] neighbors = field.GetNeighborsOf(x, y);
        float avgHeat = (neighbors[0].GetHeat() + neighbors[1].GetHeat() + neighbors[2].GetHeat() +
                         neighbors[3].GetHeat()) / 4f;
        heat = (int)(heat * 0.75f + avgHeat * 0.25);
        neighbors[0].WakeUp();
        neighbors[1].WakeUp();
        neighbors[2].WakeUp();
        neighbors[3].WakeUp();
        
        if (heat < 2) {
            ReplaceSelf<Air>();
        }
        else if (heat > 13) {
            ReplaceSelf<Fire>();
        } else {
            heat -= Random.Range(1, 1);
            velX = Mathf.Clamp(velX + Random.Range(-1, 2), -1, 1);
            velY = Mathf.Clamp(velY + Random.Range(-1, 2), 0, 4);


            if (!MoveTo<Air, Flame>(x + velX, y + velY, CreateNewElement<Air>())) {
                velX = Random.Range(-1, 2);
                velY = Random.Range(0, 4);
                MoveTo<Air, Flame>(x + velX, y + velY, CreateNewElement<Air>());
            }
        }
    }
}
