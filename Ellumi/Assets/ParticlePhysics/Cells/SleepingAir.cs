using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepingAir : Air
{
    const int MY_GRAPHIC_INDEX = 0;
    
    override public void Setup(Field field, int x = 0, int y = 0, int graphic = 0)
    {
        elementID = 2;
        base.Setup(field, x, y, MY_GRAPHIC_INDEX);
    }

    override public void ProcessCell()
    {
    }

    public override void WakeUp()
    {
        InstantReplace(CreateNewElement<Air>());
    }
    
}
