using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepingWood : Wood
{
    
    override public void Setup(Field field, int nx = 0, int ny = 0, int graphic = 0)
    {
        elementID = 5;
        this.x = nx;
        this.y = ny;
        base.Setup(field, nx, ny, 7);
        tileGraphicIndex = 7;
    }

    public override void ProcessCell()  {}

    public override void WakeUp()
    {
        InstantReplace(CreateNewElement<Wood>());
    }
}
