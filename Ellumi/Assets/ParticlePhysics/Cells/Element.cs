using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Element: ScriptableObject
{

    protected Field field;
    protected int x;
    protected int y;
    protected int heat = 0;
    protected int velX = 0;
    protected int velY = 0;
    public int tileGraphicIndex = 0;
    public int elementID = 0;
    public bool updated = false;
    public bool movedInto = false;

    public Element nextState;

    virtual public void Setup(Field field, int x = 0, int y = 0, int graphic = 0)
    {
        this.field = field;
        this.x = x;
        this.y = y;
        this.tileGraphicIndex = graphic;
        
    }
    
    void Awake()
    {
        this.nextState = this;
    }

    virtual public void ProcessCell()
    {
        // pass
    }

    public void UpdateToNextState()
    {
        if (nextState == this || updated) { return; }
        nextState.x = x;
        nextState.y = y;
        movedInto = false;
        field.ReplaceElementAtPos(nextState, x, y);
        MarkUpdated();
    }

    virtual public void WakeUp() { }

    /*
    public override string ToString()
    {
        return "Element: " + tileGraphicIndex + " at " + x + ", " + y;
    }
    */

    public Element CreateNewElement<T>() where T:Element
    {
        Element newElement = ScriptableObject.CreateInstance<T>();
        newElement.Setup(field, x, y);
        return newElement;
    }

    static public Element CreateNewElement<T>(Field field) where T:Element
    {
        Element newElement = ScriptableObject.CreateInstance<T>();
        newElement.Setup(field);
        return newElement;
    }
    
    public Element CopyOntoNewElement<T>() where T : Element
    {
        Element newElement = CreateNewElement<T>();
        newElement.SetHeat(heat);
        newElement.SetVel(velX, velY);
        return newElement;
    }
    
    public Field GetField()
    {
        return this.field;
    }
    
     

    public Element GetNextState()
    {
        return nextState;
    }

    public void ReplaceWithElement(Element newElement)
    {
        nextState = newElement;
    }

    public void InstantReplace(Element newElement)
    {
        newElement.x = x;
        newElement.y = y;
        field.ReplaceElementAtPos(newElement, x, y);
    }

    public void ReplaceSelf<T>() where T : Element
    {
        nextState = CreateNewElement<T>();
        nextState.SetHeat(heat);
        nextState.SetVel(velX, velY);
    }

   

    public bool MoveTo<TMoveTo,TMovingElement>(int x, int y, Element leaveBehind) where TMovingElement:Element
    {
        
        if (x == this.x && y == this.y) { return false;}
        Element target = field.GetElementAtPos(x, y);
        if (target is TMoveTo && !target.movedInto) {
            target.ReplaceWithElement(CopyOntoNewElement<TMovingElement>());
            target.DontMoveInto();
            ReplaceWithElement(leaveBehind);
            return true;
        }
        return false;
    }

    public void SetPos(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int GetHeat()
    {
        return heat;
    }
    
    public void SetHeat(int heat)
    {
        this.heat = heat;
    }

    public void AddHeat(int heat)
    {
        this.heat += heat;
    }

    public void SetVel(int xv, int yv)
    {
        this.velX = xv;
        this.velY = yv;
    }

    public void MarkUpdated()
    {
        updated = true;
    }

    public void UnmarkUpdated()
    {
        updated = false;
    }

    public void DontMoveInto()
    {
        movedInto = true;
    }

    public void SetField(Field field)
    {
        this.field = field;
    }
}
