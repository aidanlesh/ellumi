using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wood : Element
{
    const int MY_GRAPHIC_INDEX = 6;

    private int inactiveTurns = 0;

    override public void Setup(Field field, int x = 0, int y = 0, int graphic = 0)
    {
        elementID = 5;
        base.Setup(field, x, y, MY_GRAPHIC_INDEX);
    }

    override public void ProcessCell()
    {
        Element[] neighbors = field.GetNeighborsOf(x, y);
        float avgHeat = (neighbors[0].GetHeat() + neighbors[1].GetHeat() + neighbors[2].GetHeat() +
                         neighbors[3].GetHeat()) / 4f;
        heat = (int)(avgHeat * 0.75 + heat * 0.25);

        for (int i = 0; i < 4; i++) {
            if (neighbors[i] is Ember) {
                ReplaceSelf<Fire>();
            }
            else if (neighbors[i] is Fire && Random.value > 0.7) {
                ReplaceSelf<Flame>();
            }
        }

        if (nextState == this) {
            if (heat > 9) {
                ReplaceSelf<Ember>();
            } 
            else if (heat > 7) {
                ReplaceSelf<Fire>();
            } 
            else if (heat > 4) {
                ReplaceSelf<Flame>();
            } 
        }

        if (heat == 0) {
            inactiveTurns++;
        } else {
            inactiveTurns = 0;
        }
        
        if (inactiveTurns > 14) {
            ReplaceSelf<SleepingWood>();
            nextState.tileGraphicIndex = 6;
        }
    }
}
