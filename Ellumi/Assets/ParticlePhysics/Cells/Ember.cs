using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ember : Element
{
    const int MY_GRAPHIC_INDEX = 4;

    override public void Setup(Field field, int x = 0, int y = 0, int graphic = 0)
    {
        elementID = 4;
        heat = 17;
        base.Setup(field, x, y, MY_GRAPHIC_INDEX);
    }

    override public void ProcessCell()
    {
        Element[] neighbors = field.GetNeighborsOf(x, y);
        neighbors[0].WakeUp();
        neighbors[1].WakeUp();
        neighbors[2].WakeUp();
        neighbors[3].WakeUp();
        
        if (heat < 10) {
            ReplaceSelf<Flame>();
        }
        else if (heat < 16) {
            ReplaceSelf<Fire>();
        } else {
            heat -= 1;
            velX = Mathf.Clamp(velX + Random.Range(-1, 2), -1, 1);
            velY = Mathf.Clamp(velY + Random.Range(-1, 2), 0, 2);

            Element leaveBehind = Element.CreateNewElement<Fire>(field);
            leaveBehind.SetHeat(14);
            leaveBehind.SetVel(velX / 2, velY / 2);
            if (!MoveTo<Air, Ember>(x + velX, y + velY, leaveBehind)) {
                velX = Random.Range(-3, 4);
                velY = Random.Range(-3, 4);
                MoveTo<Flame, Ember>(x + velX, y + velY, leaveBehind);
            }
        }
    }
}