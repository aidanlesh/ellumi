using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : Element
{
    const int MY_GRAPHIC_INDEX = 3;

    override public void Setup(Field field, int x = 0, int y = 0, int graphic = 0)
    {
        elementID = 3;
        base.Setup(field, x, y, MY_GRAPHIC_INDEX);
    }

    override public void ProcessCell()
    {
        Element[] neighbors = field.GetNeighborsOf(x, y);
        neighbors[0].WakeUp();
        neighbors[1].WakeUp();
        neighbors[2].WakeUp();
        neighbors[3].WakeUp();

        if (heat > 19) {
            ReplaceSelf<Ember>();
        }
        else if (heat < 5) {
            ReplaceSelf<Air>();
        }
        else if (heat < 8) {
            ReplaceSelf<Flame>();
        } else {
            heat -= 1;
            velX = Mathf.Clamp(velX + Random.Range(-1, 2), -1, 1);
            velY = Mathf.Clamp(velY + Random.Range(-1, 2), -1, 4);

            Element leaveBehind = Element.CreateNewElement<Flame>(field);
            leaveBehind.SetHeat(8);
            leaveBehind.SetVel(velX, velY);
            if (!MoveTo<Air, Fire>(x + velX, y + velY, leaveBehind)) {
                velX = Random.Range(-3, 4);
                velY = Random.Range(-3, 4);
                MoveTo<Flame, Fire>(x + velX, y + velY, leaveBehind);
            }
        }
    }
}