using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ellumi : MonoBehaviour
{

    // these variable names give me a malicious joy
    private Rigidbody2D rb;
    private Animator an;
    private SpriteRenderer sr;
    [SerializeField] private Transform flameContainer;
    
    [SerializeField] float moveSpeed;
    [SerializeField] float jumpSpeed;
    [SerializeField] PhysicsMaterial2D staticMaterial;
    [SerializeField] PhysicsMaterial2D movingMaterial;

    [SerializeField] private List<GameObject> standingOn = new List<GameObject>();
    public bool isOnGround;
    private float jumpCooldownTimer = 0;
    private float jumpCooldown = 0.05f;
    private float coyoteTimeTimer = 0;
    [SerializeField] float coyoteTime;
    public bool isCoyote;
    public bool facingLeft = false;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        an = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Vector2 vel = rb.velocity;
        bool isVelXZero = Math.Abs(vel.x) >= 0.01;
        an.SetBool("IsRunning", isVelXZero);
        an.SetInteger("NextFlame", Math.Max(1, Random.Range(0,4))); // give flame1 double weight
        sr.flipX = facingLeft;
        // this is necessary to flip the animated positions too.
        flameContainer.localScale = facingLeft ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
    }

    void FixedUpdate()
    {
        // setup variables
        Vector2 pos = transform.position;
        Vector2 vel = rb.velocity;

        isOnGround = standingOn.Count > 0;
        if (isOnGround) {
            coyoteTimeTimer = coyoteTime;
        }
        isCoyote = coyoteTimeTimer >= 0;

        // horizontal movement
        vel.x = Input.GetAxis("Horizontal") * moveSpeed;
        if (Mathf.Abs(vel.x) > 0.04) {
            rb.sharedMaterial = movingMaterial;
            facingLeft = vel.x < 0;
        } else {
            rb.sharedMaterial = staticMaterial;
        }
        
        // jump
        if ((isOnGround || isCoyote) && Input.GetAxis("Vertical") > 0 && jumpCooldownTimer <= 0) {
            vel.y += jumpSpeed;
            jumpCooldownTimer = jumpCooldown;
            coyoteTimeTimer = -1;
        }

        // push modified variables back to where they're from
        rb.velocity = vel;
        
        // timers
        jumpCooldownTimer -= Time.fixedDeltaTime;
        coyoteTimeTimer -= Time.fixedDeltaTime;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        standingOn.Add(other.gameObject);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        standingOn.Remove(other.gameObject);
    }
}
